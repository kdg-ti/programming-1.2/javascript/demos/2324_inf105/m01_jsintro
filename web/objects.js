const wednesday = {
  lesson : true,
  events : ["wake up", "commute", "breakfast"],
  print(){ console.log(`Today there is ${this.lesson?"a":"no"} lesson`);},
  1: "one",
  "high score":50
}



console.log(wednesday);
console.log(wednesday.events);
console.log(wednesday.lesson)
wednesday.print();
wednesday.lesson = false;
wednesday.print();
wednesday.game = "fall out";
console.log(wednesday);
console.log(wednesday[1]);
console.log(wednesday["game"]);
wednesday["high score"]=100;
console.log(wednesday);
console.log("stringified " + JSON.stringify(wednesday))

console.log("logging all attributes")
for(let key in wednesday) {
  console.log(`\t${key} has value ${wednesday[key]}`);
}

myString = `{
  "lesson" : true,
    "events" : ["wake up", "commute", "breakfast"],
  "1": "one",
  "high score":50
}`;
console.log(myString);
const anotherDay=JSON.parse(myString)
console.log(anotherDay.lesson);
// typo, javascript does not warn, just returns undefined
console.log(anotherDay.Lesson);

weekDay = ["morning","noon","evening"];
weekDay.weather="sunny";
console.log("weekday: " + weekDay);

console.log("Looping over keys")
for(let key in weekDay) {
  console.log(`\t${key} has value ${weekDay[key]}`);
}
//
console.log("\nlooping over array indexes")
let index  = 0;
for(let arrayValue of weekDay) {
  console.log(`\t array position ${index++} has value ${arrayValue}`);
}