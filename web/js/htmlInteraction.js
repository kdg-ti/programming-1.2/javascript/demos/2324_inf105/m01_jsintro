const nameInput=document.getElementById("name-input");
const feedbackDiv=document.getElementById("feedback");
const feedbackDiv2=document.getElementById("feedback2");

// this only runs when the page is completely loaded
// not needed if you use modules or defer
// document.addEventListener("DOMContentLoaded", addEventHandler)
addEventHandler();

function giveFeedback(event) {
 console.log(event.target.value);
  //console.log(document.getElementById("name-input").value);
  feedbackDiv.textContent=`Hello ${event.target.value}`
  //console.log(somethingElse);
}

function addEventHandler(){
  nameInput.addEventListener("blur",giveFeedback);
  nameInput.addEventListener("blur",event => feedbackDiv2.textContent = `Goodbye ${event.target.value}`);
}

